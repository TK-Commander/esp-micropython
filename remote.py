
import json
from enum import Enum

from logging import Logger, NOTSET as logLevel
from config import configure
from device_manager import DeviceManager

log = Logger('remote commander')
log.setLevel(logLevel)

class Commander():
	def __init__(self, config):
		log.info("init")
		self.config = config
		self.manager = DeviceManager(config.get_devices(), config.get_auto_routines())
	
	async def monitor_cb(self, msg):
		msgParts = msg.decode().split('|')
		log.debug(msgParts)
		resMsg = {
			'ack': msg,
			'hostname': self.config.get_hostname(),
			'res': "message handler error"
		}
		try:
			if 'config_set' == msgParts[0]:
				# message format "config_set|JSON_to_replace_config_with"
				self.config.replace(msgParts[1]) # no responce device will reset
			elif 'config_get' == msgParts[0]:
				# message format "config_get"
				resMsg['res'] = self.config.get()
			elif 'infoReq' == msgParts[0]:
				# message format "infoReq"
				resMsg['res'] = self.manager.get_device_commands(config.get_devices(), config.get_auto_routines())
			elif 'auto_routine' == msgParts[0]:
				# message format "auto_routine|name_of_routine"
				resMsg['res'] = self.manager.run_auto_routine(config.get_auto_routine(msgParts[1]))
			else:
				# message format "componentName|cmd|value"
				resMsg['res'] = self.manager.run_device_function(msgParts)
		except KeyError:
			pass # ressponce to message is default err:msg
		log.debug(resMsg)
		return json.dumps(resMsg).encode()
