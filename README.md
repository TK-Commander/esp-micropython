# TK Commander - ESP MicroPython Controller
### Setup
- Using [MicroPython documentation](http://docs.micropython.org/en/latest/) load the latest version of Micropython onto your device.
- To load files to your device use [ampy](https://pypi.org/project/adafruit-ampy/).

## Using this project
###### Defined all capibilites in config.josn.
Refferance motor.py and led.py for pin definitions for each componant.
This step can also be done from [TK Commander UI](https://gitlab.com/TK-Commander/gui).
Place an instance of template in the approriate list.
```
networkTemplate = {
	"ssid": "Your_Network_Name",
	"password": "Your_Network_Password",
	"IP":'192.168.1.123', # Optional
	"subnet": '255.255.255.0', # Optional
	"gateway": '192.168.1.1', # Optional
	"dns": '8.8.8.8', # Optional
	"enables_webrepl": False
}
routineTemplate = {
	"name": "name_of_routine",
	"routine": ["name_of_device|go|50", "bsp|sleep|5", "name_of_device|go|0", "bsp|sleep|5", "name_of_device|break|0", "bsp|sleep|1", "name_of_device|go|0"]
}
deviceTemplate = {
	"name":"name_of_device"
	"class": "DRV8833_Single",
	"pins": (1,2,3,4), # (motor_pin1, motor_pin2, speed_pin<PWM>, standby_pin)
	"invert": False
}
```
###### Copy the files to your device
NOTE: If you do not copy config.json one will be created. Use AP and [TK Commander UI](https://gitlab.com/TK-Commander/gui) to load new config to device.
`ampy -p /dev/<your_device eg: 'tty.SLAB_USBtoUART'> put <all files>`
