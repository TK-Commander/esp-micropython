#!/usr/bin/python3

'''Convenient wrappers for simple LEDs'''

from machine import Pin, Signal
from uasyncio import sleep

from base import Momentary, ToggleSwitch

class FlashLED(FlashTimeLED):
	'''Make an LED light for a period and go off.'''
	
	def __init__(self, name, pin, invert = False):
		'''A simple LED that will flash.'''
		
		super().__init__(name, pin, invert = invert)
	
	async def trigger(self, _):
		'''Light this LED for 1s.'''
		
		return await super().trigger(1)

class FlashTimeLED(Momentary):
	'''Make an LED light for a period and go off.'''
	
	def __init__(self, name, pin, invert = False):
		'''A simple LED that will flash.'''
		
		super().__init__(name)
		self.__pin = Signal(pin, Pin.OUT, invert = invert)
	
	async def trigger(self, duration):
		'''Light this LED for `duration` seconds.'''
		
		self.__pin.on()
		await sleep(duration)
		self.__pin.off()
		return 0

class StaticLED(ToggleSwitch):
	'''A simple LED component.'''
	
	def __init__(self, name, pin, invert = False):
		'''Setup this LED.'''
		
		super().__init__(name)
		self.__pin = Signal(pin, Pin.OUT, invert = invert)
	
	def toggle(self, isSet): # ui sets state with True or False?
		'''Change the state of this LED.'''
		
		self.__pin.value(0 if isSet else 1)
		return bool(self.__pin.value())
