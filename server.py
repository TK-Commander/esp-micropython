try:
    import uselect as select
except ImportError:
    import select
try:
    import usocket as socket
except ImportError:
    import socket
try:
    import uasyncio as asyncio
except ImportError:
    import asyncio

from logging import Logger, NOTSET as logLevel
log = Logger('server')
log.setLevel(logLevel)

# UDP server
class UDP(): # https://github.com/perbu/dgram
    def __init__(self, polltimeout=1, max_packet=1024):
        self.polltimeout = polltimeout
        self.max_packet = max_packet
    
    def close(self):
        self.sock.close()

    async def serve(self, cb, host, port, backlog=5):
        log.info("start server {0}:{1}".format(host, port))
        ai = socket.getaddrinfo(host, port)[0][-1]  # blocking!
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock = s
        s.setblocking(False)
        s.bind(ai)

        p = select.poll()
        p.register(s,select.POLLIN)
        to = self.polltimeout
        while True:
            try:
                if p.poll(to):
                    buf, addr = s.recvfrom(self.max_packet)
                    ret = await cb(buf)
                    await asyncio.sleep(0)
                    if ret:
                        s.sendto(ret, addr) # blocking
                await asyncio.sleep(0)
            except asyncio.core.CancelledError:
                # Shutdown server
                s.close()
                return
    
    # async def send(self, message, addr): # non-blocking?
    #     # s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #     # await s.sendto(message, (addr, 44445))
    #     await self.sock.sendto(message, (addr, 44445))
