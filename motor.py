#!/usr/bin/python3

'''A variety of PWM motors.'''

from math import floor
from time import sleep

from machine import Pin, PWM, Signal

from base import Motor

class TB6612FNG_Single(Motor):
	'''A single motor driven by a TB6612FNG.'''
	
	def __init__(self, name, pins, direction = Motor.Direction.NORMAL):
		'''
		Setup this motor instance.
		`pins` is expected to be a 4-tuple like:
			(motor_pin1, motor_pin2, speed_pin<PWM>, standby_pin)
		'''
		
		super().__init__(name)
		self.__speed_pin = PWM(Pin(pins[2], Pin.OUT), freq=500, duty_u16=0)
		self.__standby_pin = Signal(pins[3], Pin.OUT)
		if direction is Motor.Direction.NORMAL:
			self.__forward_pin = Signal(pins[0], Pin.OUT)
			self.__backward_pin = Signal(pins[1], Pin.OUT)
		else:
			self.__forward_pin = Signal(pins[1], Pin.OUT)
			self.__backward_pin = Signal(pins[0], Pin.OUT)
	
	def go(self, value):
		'''
		Instruct the motor to go in the given direction at a given speed.
		Expected range => [-100, 100].
		'''
		# set status here
		self.__standby_pin.on()
		if value == 0:
			self.__forward_pin.off()
			self.__backward_pin.off()
		elif value > 0:
			self.__forward_pin.on()
			self.__backward_pin.off()
		else:
			self.__forward_pin.off()
			self.__backward_pin.on()
		self.__speed_pin.duty_u16(min(floor((value + 100) * 327.68), 65535))
		return value # use status here
	
	def brake(self, _):
		'''Prevent the axle from turning.'''
		
		# set status to 0
		self.__standby_pin.on()
		self.__forward_pin.on()
		self.__backward_pin.on()
		self.__speed_pin.duty_u16(65535 // 2)
		# sleep(5) # timeout the brake function to not over load the controler
		# return self.go(self.status) # use status here to set the go value to what ever curent is

class DRV8833_Single(Motor):
	'''A single motor driven by a DRV8833 or DRV8837.'''
	
	def __init__(self, name, pins, direction = Motor.Direction.NORMAL):
		'''
		Setup this motor instance.
		`pins` is expected to be a 3-tuple like:
			(motor_pin1<PWM>, motor_pin2<PWM>, standby_pin)
		'''
		
		super().__init__(name)
		self.__standby_pin = Signal(pins[2], Pin.OUT)
		frequency = 500
		if direction is Motor.Direction.NORMAL:
			self.__forward_pin = PWM(Pin(pins[0], Pin.OUT), freq=frequency, duty_u16=0)
			self.__backward_pin = PWM(Pin(pins[1], Pin.OUT), freq=frequency, duty_u16=0)
		else:
			self.__forward_pin = PWM(Pin(pins[1], Pin.OUT), freq=frequency, duty_u16=0)
			self.__backward_pin = PWM(Pin(pins[0], Pin.OUT), freq=frequency, duty_u16=0)
	
	def go(self, value):
		'''
		Instruct the motor to go in the given direction at a given speed.
		Expected range => [-100, 100].
		'''
		# set status here
		self.__standby_pin.on()
		duty = min(floor((value + 100) * 327.68), 65535)
		if value == 0:
			self.__forward_pin.duty_u16(0)
			self.__backward_pin.duty_u16(0)
		elif value > 0:
			self.__forward_pin.duty_u16(duty)
			self.__backward_pin.duty_u16(0)
		else:
			self.__forward_pin.duty_u16(0)
			self.__backward_pin.duty_u16(duty)
		return value # use status here
	
	def brake(self, _):
		'''Prevent the axle from turning.'''
		
		# set status to 0
		self.__standby_pin.on()
		duty = 65535 // 2
		self.__forward_pin.duty_u16(duty)
		self.__backward_pin.duty_u16(duty)
		# sleep(5) # timeout the brake function to not over load the controler
		# return self.go(self.status)
		
class L9110(Motor):
	'''A single motor driven by a L9110.'''
	
	def __init__(self, name, pins, direction = Motor.Direction.NORMAL):
		'''
		Setup this motor instance.
		`pins` is expected to be a 3-tuple like:
			(motor_pin1<PWM>, motor_pin2<PWM>)
		'''
		
		super().__init__(name)
		self.__standby_pin = Signal(pins[2], Pin.OUT)
		frequency = 500
		if direction is Motor.Direction.NORMAL:
			self.__forward_pin = PWM(Pin(pins[0], Pin.OUT), freq=frequency, duty_u16=0)
			self.__backward_pin = PWM(Pin(pins[1], Pin.OUT), freq=frequency, duty_u16=0)
		else:
			self.__forward_pin = PWM(Pin(pins[1], Pin.OUT), freq=frequency, duty_u16=0)
			self.__backward_pin = PWM(Pin(pins[0], Pin.OUT), freq=frequency, duty_u16=0)
	
	def go(self, value):
		'''
		Instruct the motor to go in the given direction at a given speed.
		Expected range => [-100, 100].
		'''
		# set status here
		duty = min(floor((value + 100) * 327.68), 65535)
		if value == 0:
			self.__forward_pin.duty_u16(0)
			self.__backward_pin.duty_u16(0)
		elif value > 0:
			self.__forward_pin.duty_u16(duty)
			self.__backward_pin.duty_u16(0)
		else:
			self.__forward_pin.duty_u16(0)
			self.__backward_pin.duty_u16(duty)
		return value # use status here
	
	def brake(self, _):
		'''Prevent the axle from turning.'''
		
		# set status to 0
		duty = 65535 // 2
		self.__forward_pin.duty_u16(duty)
		self.__backward_pin.duty_u16(duty)
		# sleep(5) # timeout the brake function to not over load the controler
		# return self.go(self.status)
