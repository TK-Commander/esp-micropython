#!/usr/bin/python3

'''Base classes for components.'''

from enum import Enum

class ComponentType(Enum):
	'''Indicator of the value a function can take.'''
	TRIGGER = 1 # peramitor is ignored
	TOGGLE = 2 # peramitor is True or False, 0 or 1
	ANALOG = 3 # peramitor is  [-100, 100]
	LEVEL = 4 # peramitor is [0, 100]
	
class Base():
	'''Base class for all components.'''
	
	def __init__(self, name):
		'''Setup this component.'''
		
		super().__init__()
		self.__name = name
	
	def name(self, _):
		'''Get the given name of this component.'''
		
		return self.__name
	
	def functions(self):
		'''Get the abilities of this component.'''
		
		return  {
			# special: name is the only element of functions that is not a ComponentType
			self.name.__name__: self.__name,
		}
		
class Motor(Base):
	'''Base class for motors.'''
	
	class Direction(Enum):
		'''Indicator for a reverse function motor.'''
		NORMAL = False
		REVERSE = True
	
	def go(self, value):
		'''Virtual method to rotate a motor.'''
		
		raise NotImplementedError()
	
	def brake(self, value):
		'''Virtual method to prevent a motor from rotating.'''
		
		raise NotImplementedError()
	
	def functions(self):
		'''Get the abilities of this motor.'''
		
		return dict(**super().functions(), **{
			self.go.__name__: ComponentType.ANALOG,
			self.brake.__name__: ComponentType.TRIGGER,
		})

class Momentary(Base):
	'''Base class for momentary actions.'''
	
	async def trigger(self, _):
		'''Virtual method to activate a momentary action.'''
		
		raise NotImplementedError()
	
	def functions(self):
		'''Get the abilities of this momentary action.'''
		
		return dict(**super().functions(), **{
			self.trigger.__name__:  ComponentType.TRIGGER,
		})

class ToggleSwitch(Base):
	'''Base class for toggle-able actions.'''
	
	def toggle(self, _):
		'''Virtual method to change the state of this boolean switch.'''
		
		raise NotImplementedError()
	
	def functions(self):
		'''Get the abilities of this toggle-able action.'''
		
		return dict(**super().functions(), **{
			self.toggle.__name__:  ComponentType.TOGGLE,
		})
