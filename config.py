#add information to this config file manager as needed
import json

from logging import Logger, NOTSET as logLevel

log = Logger('config')
# log.setLevel(logLevel)

___networkTemplate = {
	"ssid": "Your_Network_Name",
	"password": "Your_Network_Password",
	"IP":'192.168.1.123', # Optional
	"subnet": '255.255.255.0', # Optional
	"gateway": '192.168.1.1', # Optional
	"dns": '8.8.8.8', # Optional
	"enables_webrepl": False
}

___commandTemplate = "name_of_device:function_to_call:value"
___routineTemplate = {
	"name": "name_of_routine",
	"routine": ["name_of_device:go:50", "bsp:sleep:5", "name_of_device:go:0", "bsp:sleep:5", "name_of_device:break:0", "bsp:sleep:1", "name_of_device:go:0"]
}
___deviceTemplate = [
	{
		"name":"name_of_device",
		"class": "NAME_OF_DEVICE_CLASS",
		"pins": [1,2,3], # review class documentation for pins definitions
		"invert": False # LED (alwas on) or Motor (turning the other direction)
	}
]

_default = {
	"hostname": "ESP-config",
	"access_point": {
		"essid": "ESP-pass Config AP",
		"password": "ESP-pass",
		"enables_webrepl": True,
	},
	"known_networks": [], # populated by line 84 of this file
	"auto_routine": [],
	"devices": []
}

class configure():
	config_file = 'config.json'
	config = None   
	
	@classmethod # base
	def load(self):
		if self.config is None:
			try:
				with open(self.config_file, "r") as f:
					log.info('config load json')
					self.config = json.loads(f.read())
					log.debug(self.config)
				return
			except Exception as e:
				log.critical("Config FIle Missing {0}".format(e))
				self.config = _default
				self.save()
				log.critical("Creating new File and reseting")
				import machine
				machine.reset()
		else:
			log.info('already loaded')

	@classmethod
	def get(self) -> dict:
		self.load()
		return self.config
	
	@classmethod
	def replace(self, newConfig):
		with open(self.config_file, 'w') as f:
			log.info('save config')
			json.dump(json.loads(newConfig), f)
		log.critical("new Configuration recived, reseting")
		import machine
		machine.reset()
	
	# Extra helper functions for local use only
	@classmethod
	def get_hostname(self, NonOpt=None) -> str:
		self.load()
		log.info('get hostname')
		try:
			return self.config['hostname']
		except KeyError:
			return None
	
	@classmethod
	def get_known_networks(self, NonOpt=None) -> list:
		self.load()
		log.info('get networks')
		try:
			return self.config['known_networks']
		except KeyError:
			return []
	
	@classmethod
	def get_config_ap(self) -> dict:
		self.load()
		try:
			return self.config['access_point']
		except KeyError:
			return {}

	@classmethod
	def get_auto_routines(self) -> dict:
		self.load()
		try:
			return self.config['auto_routine']
		except KeyError:
			return {}

	@classmethod
	def get_auto_routine(self, name) -> list:
		self.load()
		try:
			return self.config['auto_routine'][name]
		except KeyError:
			return []

	@classmethod
	def get_devices(self) -> list:
		self.load()
		log.info('get devices')
		try:
			return self.config['devices']
		except KeyError:
			return []
