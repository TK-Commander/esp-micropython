
import network
from time import sleep

from config import configure
from logging import Logger, INFO as logLevel

log = Logger('wifi')
log.setLevel(logLevel) # move logLevel into config?

def STA_IF():
    return network.WLAN(network.STA_IF)

def AP_IF():
    return network.WLAN(network.AP_IF)

def MY_IP():
    return STA_IF().ifconfig()[0]

def ifREPL(enabled):
    if enabled:
        try:
            import webrepl
            webrepl.start()
        except NameError:
            # Log error here (not after import) to not log it if webrepl is not configured to start.
            log.critical("Failed to start webrepl, module is not available.")

class wifi():
    def __init__(self, configure):
        if not self.connect():
            self.setupAP()
        log.info("HOSTNAME: {0}\n\t{1}".format(STA_IF().config('dhcp_hostname'), STA_IF().ifconfig()))
    
    async def reconnect(cls):
        try:
            import uasyncio as asyncio
        except ImportError:
            import asyncio
        while True:
            if STA_IF().isconnected():
                await asyncio.sleep(30)
            else:
                cls.setupAP()
    
    def connect(self) -> bool:
        known_networks = self.configure.get_known_networks()
        # get avalible wifi networks
        available_networks = []
        STA_IF().active(True)
        STA_IF().config(dhcp_hostname=self.configure.get_hostname())

        for network in STA_IF().scan():
            available_networks.append(network[0].decode("utf-8"))

        log.debug("available_networks {}".format(available_networks))
        # if a known network is in the avalible network list conect to it
        for known in known_networks:
            log.debug("network: {0}, pass: {1}".format(known['ssid'], known['password']))
            if known['ssid'] in available_networks:
                try:
                    STA_IF().ifconfig((known['IP'], known['subnet'], known['gateway'], known['dns']))
                except KeyError:
                    log.debug("no static configureation for this network")
                STA_IF().connect(known['ssid'], known['password'])
                log.info("Wifi Connecting...")
                from time import sleep
                while not STA_IF().isconnected():
                    sleep(1)
                
                log.info("Wifi Connected {}".format(known['ssid']))
                ifREPL(known['enables_webrepl'])
                return True
        return False

    def setupAP(self):
        log.info('Wifi AP Setup...')
        ap_authmode = 3  # WPA2
        ap_config = self.configure.get_config_ap()
        # if ap_config.
        AP_IF().active(True)

        AP_IF().config(essid=ap_config['essid'], password=ap_config['password'], authmode=ap_authmode)
        
        log.info('Wifi AP Avalible at {}'.format(ap_config['essid']))
        ifREPL(ap_config['enables_webrepl'])
