
from config import configure
conf = configure()

from wifi import wifi, MY_IP
wifi(conf)

from remote import Commander
cmd = Commander(conf)

from server import UDP
udp = UDP()

try:
    import uasyncio as asyncio
except ImportError:
    import asyncio

asyncio.get_event_loop().run_until_complete(udp.serve(cmd.monitor_cb, MY_IP(), 44444))
asyncio.get_event_loop().run_forever()
