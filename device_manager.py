'''Device Manager'''

from time import sleep

import motor
import led
from base import Base, ComponentType

from logging import Logger, INFO as logLevel

log = Logger('Device Manager')
log.setLevel(logLevel) # move logLevel into config?

class DeviceManager():
	
	def __init__(self, config_devices, auto_routines):
		bsp = BSP()
		auto = AUTO(auto_routines)
		self._active_devices = { bsp.name(None): bsp, auto.name(None): auto }
		
		for device in config_devices:
			# Motors
			if "TB6612FNG_Single" == device["class"]:
				pass
				self._active_devices[device["name"]] = motor.TB6612FNG_Single(device["name"], device["pins"], device['invert'])
			elif "DRV8833_Single" == device["class"]:
				pass
				self._active_devices[device["name"]] = motor.DRV8833_Single(device["name"], device["pins"], device['invert'])
			elif "L9110" == device["class"]:
				pass
				self._active_devices[device["name"]] = motor.L9110(device["name"], device["pins"], device['invert'])
			# LEDs
			elif "FlashLED" == device["class"]:
				pass
				self._active_devices[device["name"]] = led.FlashLED(device["name"], device["pins"], device['invert'])
			elif "FlashTimeLED" == device["class"]:
				pass
				self._active_devices[device["name"]] = led.FlashTimeLED(device["name"], device["pins"], device['invert'])
			elif "StaticLED" == device["class"]:
				pass
				self._active_devices[device["name"]] = led.StaticLED(device["name"], device["pins"], device['invert'])
			
			else:
				log.critical("device class: {0}, not found.".format(device["class"]))
	
	def run_device_function(self, cmd):
		try:
			return getattr(self._active_devices[cmd[0]], cmd[1])(cmd[2])
		except KeyError:
			return "no device = {0}".format(cmd[0])
		except AttributeError:
			return "no ability = {0}".format(cmd[1])
		except IndexError:
			return "command error, component_name:function:value"
	
	def run_auto_routine(self, routine):
		for cmd in routine:
			self.run_device_function(cmd)
	
	def get_device_commands(self):
		cmd_list = []
		for device in self._active_devices.values():
			cmd_list.append(device.functions())
		return cmd_list


class BSP(Base):
	'''Board support Package'''
	
	def __init__(self):
		'''Board support Package'''
		
		super().__init__('bsp')
	
	def sleep(self, value):
		'''Sleep for a given time'''
		sleep(value)
	
	def functions(self):
		'''Get the abilities of configure.'''
		
		return dict(**super().functions(), **{
			self.sleep.__name__: ComponentType.TRIGGER
		})

class AUTO(Base):
	'''Board support Package'''
	def __init__(self, auto_routines):
		'''Board support Package'''
		
		super().__init__('auto_routine')
		self.auto = {}
		for key in auto_routines.keys():
			self.auto[key] = ComponentType.TRIGGER
	
	def functions(self):
		'''Get the abilities of configure.'''
		
		return dict(**super().functions(), **self.auto)
